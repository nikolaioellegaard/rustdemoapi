FROM rustlang/rust:nightly-buster-slim as builder


# Download the target for static linking.
RUN rustup target add x86_64-unknown-linux-musl

WORKDIR /usr/src/rocket-api-demo
COPY . .

RUN cargo build --release
RUN cargo install --target x86_64-unknown-linux-musl --path .

FROM busybox:1.32

COPY --from=builder /usr/local/cargo/bin/rocket-api-demo /usr/local/bin/rocket-api-demo

ENV ROCKET_PORT=5000
EXPOSE 5000
HEALTHCHECK CMD wget -q -O /dev/null http://localhost5000 || exit 1

CMD ["rocket-api-demo"]
