#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_okapi;

use rocket_contrib::json::Json;
use rocket_okapi::swagger_ui::*;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, JsonSchema)]
struct Category {
    id: u64,
    name: String
}

#[derive(Serialize, Deserialize, JsonSchema)]
struct Pet {
    id: u64,
    category: Category,
    name: String,

}

/// # Get Pet
///
/// Returns a single pet by ID.
#[openapi]
#[get("/pet/<id>")]
fn index(id: u64) -> Json<Pet> {
    let pet = Pet { 
        id: id, 
        name: String::from("Kitty"),
        category: Category { id: 2, name: String::from("Indoor") }
    };
    Json(pet)
}


fn main() {
    rocket::ignite()
    .mount(
        "/api", 
        routes_with_openapi![
            index,
        ],
    )
    .mount(
        "/",
        make_swagger_ui(&SwaggerUIConfig {
            url: "/api/openapi.json".to_owned(),
            ..Default::default()
        }),
    )
    .launch();
}
